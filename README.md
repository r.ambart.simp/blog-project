# **Blog-Symfony**

 est un blog developper avec php symfony pour un projet d'étude. Developper en 2 semaines , les fonctionalités principales d'un blog classic ont été implementées:

- Ajouts et affichage des articles
- Modification et suppression des articles
- Une barre de recherche

Je vous invites a tester cette premiere version.

##  Présentation du projet

Le but de ce projet est d'utiliser les competences aquises avec **SYMFONY 4** durant les cours, et d'en developper de nouvelles grace a l'auto-formation. j'ai également utilisé **DOCTRINE** pour ce projet. Dans cet esprit j'ai entrepris un blog avec une vue plutot axé *administrateur* ou il est possible (pour l'admin) d'ajouter des articles, les modifiées, les supprimées (**CRUD**).

##  Languages utilisées

- **HTML/CSS**
- **Bootstrap**
- **MYSQL**
- **Javascript**
- **PHP 7**

##  

##  ScreenShot du blog:

![img](/r.ambart.simp/project-game-js/raw/master/screen1.png)

![img](/r.ambart.simp/project-game-js/raw/master/screen2.png)



##  Version.

Actuellement en version 0.9.7.

Des améliorations et des nouvelles ajouts sont susceptible d'etre apporter prochainement comme:

- Plus d'optimisations et de fluidité.
- Nouveaux ennemies
- Plusieurs niveaux de jeux.

##  Auteur.

[@Rapha]() , formation Simplon.SE , promo9.

##  **Contact**.

Pour plus d'information ou de simple questions n'hesitez pas a me contactez via mail : [r.ambart.simp@gmail.com](mailto:r.ambart.simp@gmail.com)

Vous pouvez aussi me retrouvez sur Simplonlyon.fr.