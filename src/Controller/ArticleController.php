<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ArticleType;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;



class ArticleController extends AbstractController {

    /**
     * @Route("/add-article", name="add_article")
     */

    public function index(Request $request, ArticleRepository $repo) {
        
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
        
            $repo->add($article);
            return $this->redirectToRoute('home');

        }
       
        return $this->render("add-article.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route ("/article/{id}", name="show_article")
     */
    public function oneArticle(ArticleRepository $repo, int $id){
        return $this->render("article.html.twig", [
            "article" => $repo->find($id)
        ]);
    }

       /**
     * @Route("/all-articles", name="all_articles")
     */

    public function allArticles(ArticleRepository $repo) {
        
        return $this->render("all-articles.html.twig", [
      
            "articles" => $repo->findAll()

        ]);
    }

    /**
     * @Route ("/article/edit/{id}", name="edit_article")
     */
    public function Edits(int $id, ArticleRepository $repo,Request $request){

        $oldResult = $repo->find($id);

        $form = $this->createForm(ArticleType::class, $oldResult);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
        
            $repo->update($oldResult);
            return $this->redirectToRoute('show_article', [
               "id"=>$id 
            ]);
        }

        return $this->render("edit-article.html.twig", [
            "result"=>$oldResult,
            "form" =>$form->createView()
        ]);
    }
    /**
     * @Route ("/remove/{id}", name="remove_article")
     */

    public function remove(int $id, ArticleRepository $repo) {

        $repo->remove($repo->find($id));

        return $this->redirectToRoute('home');
}
/**
 * @Route ("/" , name="home")
 */

public function news(ArticleRepository $repo) {
      
    return $this->render("index.html.twig", [
        "articles" => $repo->newArticle()

    ]);
}

/**
 * @Route ("/search" , name="search")
 */
public function searchArticle(Request $request, ArticleRepository $repo){

    $result = $repo->search($request->get('search'));

    return $this->render("search.html.twig", [
        "search" => $result

    ]);
}
}