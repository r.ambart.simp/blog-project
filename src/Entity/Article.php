<?php

namespace App\Entity;

class Article {
    
    public $id;
    public $title;
    public $descriptions;
    public $dates;
    public $content;
    public $author;

}