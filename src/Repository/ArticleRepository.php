<?php

namespace App\Repository;

use App\Entity\Article;

class ArticleRepository {

    /**
     * 
     * @return Article[] 
     */
    public function findAll():array {
        $articles = [];

        $connection = DbConnexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM articles");
        $query->execute();
    
        $results = $query->fetchAll();
        foreach ($results as $line) {
            $articles[] = $this->sqlToArticle($line);
        }
        return $articles;
    }

    private function sqlToArticle(array $line): Article {
        $article = new Article();
    
        $article->id = intval($line["id"]);
        $article->title = $line["title"];
        $article->descriptions = $line["descriptions"];
        $article->dates = ($line["dates"]);
        $article->content = ($line["content"]);
        $article->author=($line["author"]);
        return $article;
    }

    public function add(Article $article): void {
        $connection = DbConnexion::getConnection();
     
        $query = $connection->prepare("INSERT INTO articles (title,descriptions,dates,content,author) VALUES (:title,:descriptions,NOW(),:content, :author)");
   
        $query->bindValue(":title", $article->title);
        $query->bindValue(":descriptions", $article->descriptions);
        $query->bindValue(":content", $article->content);
        $query->bindValue(":author", $article->author);
        $query->execute();

    
        $article->id = $connection->lastInsertId();
    }


    public function find(int $id): ?Article {
        $connection = DbConnexion::getConnection();
   
        $query = $connection->prepare("SELECT * FROM articles WHERE id=:id");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

   
        if($line = $query->fetch()) {
         
            return $this->sqlToArticle($line);

        }
        return null;
    }

    public function update(Article $article): void {
        $connection = DbConnexion::getConnection();

        $query = $connection->prepare("UPDATE articles SET title=:title, descriptions=:descriptions,content=:content WHERE id=:id");
        $query->bindValue(":title", $article->title);
        $query->bindValue(":descriptions", $article->descriptions);
        $query->bindValue(":content", $article->content);
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();

    }

    public function remove(Article $article): void {
        $connection = DbConnexion::getConnection();

        $query = $connection->prepare("DELETE FROM articles WHERE id=:id");
        $query->bindValue(":id", $article->id, \PDO::PARAM_INT);

        $query->execute();
    }

    public function newArticle():array {
        $articles = [];

        $connection = DbConnexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM articles order by id desc LIMIT 6 ");
        $query->execute();
    
        $results = $query->fetchAll();
        foreach ($results as $line) {
            $articles[] = $this->sqlToArticle($line);
        }
        return $articles;
    }
    public function search(string $article){

        $articles = [];

        $connection = DbConnexion::getConnection();
        
        $query = $connection->prepare("SELECT * FROM articles WHERE title LIKE :words");
        $query->bindValue(':words' , "%" . $article ."%" );
        $query->execute();

        $result = $query ->fetchAll();

        foreach ($result as $line) {

            $articles[] = $this->sqlToArticle($line);

        }
        return $articles;

    }
    
}