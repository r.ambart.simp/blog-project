<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;



class ArticleType extends AbstractType {

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add("author", TextType::class, array('label'=>'Auteur'))
        ->add("title", TextType::class, array('label'=>'Titre'))
        ->add("descriptions", TextType::class, array('label'=>'Description'))
        ->add("content", TextareaType::class, array('label'=>'Contenue'));
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}